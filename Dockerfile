FROM adoptopenjdk/openjdk11:jre11u-ubuntu-nightly
ADD target/demo-0.0.1-SNAPSHOT.jar .
EXPOSE 9091
CMD java -jar demo-0.0.1-SNAPSHOT.jar