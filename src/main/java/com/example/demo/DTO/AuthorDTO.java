package com.example.demo.DTO;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthorDTO {
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private String email;
}
