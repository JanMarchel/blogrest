package com.example.demo.DTO;


import lombok.Data;

import java.util.Collection;

@Data
public class PostDTO {
    private String content;
    private String tags;
    private Collection<String> authors;
}
