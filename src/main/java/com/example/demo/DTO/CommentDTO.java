package com.example.demo.DTO;

import lombok.Data;

@Data
public class CommentDTO {
    private String username;
    private String content;
    private long postId;
}
