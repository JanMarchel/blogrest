package com.example.demo.service;

import com.example.demo.DTO.AuthorDTO;
import com.example.demo.model.Author;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public interface AuthorService extends UserDetailsService {
    AuthorDTO mapToAuthorDto(Author author);
    Author findById(long id);
    Optional<Author> getAuthorByUsername(String username);

    Author mapToDtoAuthor(AuthorDTO authorDTO);
}
