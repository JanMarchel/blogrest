package com.example.demo.service;

import com.example.demo.DTO.PostDTO;
import com.example.demo.model.Post;

import java.util.List;
import java.util.Optional;

public interface PostService {
    void update(Post post, PostDTO postDTO);
    Post findById(long postId);
    PostDTO mapToPostDTO(Post post);
    List<PostDTO> getAllUserPosts(String username);
    Optional<PostDTO> addTagToPost(long id, String tag);
}
