package com.example.demo.service;

import com.example.demo.DTO.CommentDTO;
import com.example.demo.DTO.PostDTO;
import com.example.demo.model.Author;
import com.example.demo.model.Comment;
import com.example.demo.model.Post;
import com.example.demo.repository.AuthorRepository;
import com.example.demo.repository.PostRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.stream.Collectors;
@Service
@AllArgsConstructor
public class MapService {
    @Autowired
    private AuthorRepository authorRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private PostService postService;

    public Post mapToPost(PostDTO postDTO) {

       List<Author> authorList = authorRepository.findAll()
               .stream().filter(x ->  postDTO.getAuthors()
                       .contains(x.getUsername())).collect(Collectors.toList());

        return Post.builder()
                .content(postDTO.getContent())
                .tags(postDTO.getTags()).authors(authorList)
                .build();
        }

    public Comment mapToComment(CommentDTO commentDTO) {
        Post post = postService.findById(commentDTO.getPostId());
        return Comment.builder()
                .content(commentDTO.getContent())
                .username(commentDTO.getUsername())
                .post(post)
                .build();
    }

    public CommentDTO mapToCommentDTO(Comment comment) {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setContent(comment.getContent());
        commentDTO.setUsername(comment.getUsername());
        commentDTO.setPostId(comment.getPost().getId());
        return commentDTO;
    }
}
