package com.example.demo.service;

import com.example.demo.model.Post;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface AttachmentService {
     void  saveAttachment(MultipartFile file, Post post);
}
