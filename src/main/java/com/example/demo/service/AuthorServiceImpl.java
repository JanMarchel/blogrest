package com.example.demo.service;

import com.example.demo.DTO.AuthorDTO;
import com.example.demo.ResouceNotFoundException;
import com.example.demo.model.Author;
import com.example.demo.model.Role;
import com.example.demo.repository.AuthorRepository;
import lombok.AllArgsConstructor;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private AuthorRepository authorRepository;

    @Override
    public AuthorDTO mapToAuthorDto(Author author) {
        return AuthorDTO.builder().email(author.getEmail())
                .username(author.getUsername())
                .firstname(author.getFirstname())
                .password(author.getPassword())
                .lastname(author.getLastname())
                .build();
    }

    @Override
    public Author findById(long id) {
        return authorRepository.findById(id)
                .orElseThrow(() -> new ResouceNotFoundException("Author not found for this id:" + id));
    }
    public Optional<Author> getAuthorByUsername(String username){
        return authorRepository.findByUsername(username);
    }

    @Override
    public Author mapToDtoAuthor(AuthorDTO authorDTO) {
        return Author.builder().email(authorDTO.getEmail())
                .firstname(authorDTO.getFirstname())
                .lastname(authorDTO.getLastname())
                .password(authorDTO.getPassword())
                .username(authorDTO.getUsername())
                .build();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Author author = authorRepository.findByUsername(username)
                .orElseThrow(() -> new ResouceNotFoundException("User not found"));
        if(author == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(author.getUsername(), author.getPassword(), mapRolesToAuthorities(author.getRoles()));
    }
    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles){
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
    }
}


