package com.example.demo.service;

import com.example.demo.DTO.CommentDTO;
import com.example.demo.ResouceNotFoundException;
import com.example.demo.model.Comment;
import com.example.demo.repository.CommentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CommentServiceImpl implements CommentService {
    private CommentRepository commentRepository;
    private AuthorService authorService;
    private MapService mapService;


    @Override
    public Comment findById(long commentId) {
        return commentRepository.findById(commentId)
                .orElseThrow(() ->new ResouceNotFoundException("Comment not found for this id:" + commentId));
    }

    public List<CommentDTO> getAllUserComments (String username){
        if(authorService.getAuthorByUsername(username).isEmpty())
            throw new ResouceNotFoundException("User not found for this username");
        return commentRepository.findAllByUsername(username)
                .stream()
                .map(x -> mapService.mapToCommentDTO(x))
                .collect(Collectors.toList());
    }

    public List<Comment> getCommentsByUsername(String username){
        return commentRepository.findAllByUsername(username);
    }
}
