package com.example.demo.service;

import com.example.demo.DTO.PostDTO;
import com.example.demo.ResouceNotFoundException;
import com.example.demo.model.Author;
import com.example.demo.model.Post;
import com.example.demo.repository.AuthorRepository;
import com.example.demo.repository.PostRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PostServiceImpl implements PostService {
    private PostRepository postRepository;
    private AuthorRepository authorRepository;
    private AuthorService authorService;

    @Override
    @CachePut(value = "posts",key = "#post.id")
    public void update(Post post, PostDTO postDTO) {
        post.setTags(postDTO.getTags());
        post.setContent(postDTO.getContent());
        List<Author> authors = authorRepository.findAll();
        List<Author> result = new ArrayList<>();

        for (Author author: authors
             ) {
            for (String username: postDTO.getAuthors()) {
                if (author.getUsername().equals(username)) result.add(author);
            }
        }
        post.setAuthors(result);
    }

    public PostDTO mapToPostDTO(Post post) {
        List<String> usernames = new ArrayList<>();
        for (Author author: post.getAuthors()
        ) {
            usernames.add(author.getUsername());
        }
        PostDTO postDTO = new PostDTO();
        postDTO.setAuthors(usernames);
        postDTO.setContent(post.getContent());
        postDTO.setTags(post.getTags());
        return postDTO;
    }
    private String removeTagDuplicates(String tags){
        List <String> list = Arrays.asList(tags.split(" ").clone());
        List<String> deduped = list.stream().distinct().collect(Collectors.toList());
        return String.join(" ", deduped);
    }

    public Optional<PostDTO> addTagToPost(long id,String tag){
        Post post = findById(id);
        String newTags = post.getTags() + " " + tag;
        post.setTags(removeTagDuplicates(newTags.replaceAll("[\"]","")));

        postRepository.save(post);

        return Optional.of(post).map(this::mapToPostDTO);
    }

    @Override
    public Post findById(long id) {
        return postRepository.findById(id)
                .orElseThrow(() -> new ResouceNotFoundException("Post not found for id" + id));
    }

    public List<PostDTO> getAllUserPosts (String username){
        if(authorService.getAuthorByUsername(username).isEmpty())
            throw new ResouceNotFoundException("User not found for this username:" + username);
        return postRepository.findAll().stream()
                .filter(p -> p.getAuthors().contains(authorService.getAuthorByUsername(username).get()))
                .map(this::mapToPostDTO)
                .collect(Collectors.toList());
    }
}

