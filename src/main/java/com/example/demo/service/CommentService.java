package com.example.demo.service;


import com.example.demo.DTO.CommentDTO;
import com.example.demo.model.Comment;
import com.example.demo.model.Post;

import java.util.Arrays;
import java.util.List;

public interface CommentService {
    Comment findById(long commentId);
    List<CommentDTO> getAllUserComments (String username);
}
