package com.example.demo.repository;

import com.example.demo.model.Comment;
import com.example.demo.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;
import java.util.List;

public interface CommentRepository extends JpaRepository<Comment,Long> {
    @Query("SELECT p from Comment p where p.username like %?1%")
    List<Comment> findCommentsByUsername(String username);
    @Query("SELECT DISTINCT username FROM Comment")
    ArrayList<String> findAllUsernames();
    List<Comment> findAllByUsername(String username);
}
