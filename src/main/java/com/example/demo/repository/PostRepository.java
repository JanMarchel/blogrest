package com.example.demo.repository;

import com.example.demo.model.Post;
import org.springframework.cache.annotation.CachePut;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;
import java.util.List;

public interface PostRepository extends JpaRepository<Post,Long> {
    @Query("SELECT p from Post p where p.tags like %?1%")
    List<Post> findByTagsLike(String s);
    @Query("SELECT p from Post p where p.content like %?1%")
    List<Post> findByWordLike(String s);
}
