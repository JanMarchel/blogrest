package com.example.demo.Config;


import com.example.demo.model.Author;
import com.example.demo.model.Role;
import com.example.demo.repository.AuthorRepository;
import com.example.demo.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Collections;

@Component
public class SetupDataLoader implements
        ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private AuthorRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RoleRepository roleRepository;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {

        createRoleIfNotFound();
        Collection<Author> authors = userRepository.findAll();
        for (Author author : authors) {
            Collection<Role> roles = author.getRoles();
            for (Role role : roles
            ) {
                if (role.getName().equals("ROLE_ADMIN")) return;
            }
        }

        Role adminRole = roleRepository.findByName("ROLE_ADMIN");
        Author author = new Author();
        author.setUsername("test");
        author.setFirstname("Test");
        author.setLastname("Test");
        author.setPassword(passwordEncoder.encode("test"));
        author.setEmail("test@test.com");
        author.setRoles(Collections.singletonList(adminRole));
        userRepository.save(author);
    }

    @Transactional
    Role createRoleIfNotFound() {

        Role role = roleRepository.findByName("ROLE_ADMIN");
        if (role == null) {
            role = new Role("ROLE_ADMIN");
            roleRepository.save(role);
        }
        return role;
    }
}