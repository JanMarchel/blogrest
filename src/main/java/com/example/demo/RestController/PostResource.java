package com.example.demo.RestController;


import com.example.demo.DTO.PostDTO;
import com.example.demo.ResouceNotFoundException;
import com.example.demo.model.Post;
import com.example.demo.repository.AuthorRepository;
import com.example.demo.repository.PostRepository;
import com.example.demo.service.AuthorService;
import com.example.demo.service.MapService;
import com.example.demo.service.PostService;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/post")
@AllArgsConstructor
public class PostResource {

    private PostService postService;
    private PostRepository postRepository;
    private AuthorService authorService;
    private MapService mapService;
    private AuthorRepository authorRepository;

    @GetMapping("/all/content/{sortDir}")
    public ResponseEntity<List<PostDTO>> getAllPostsCa(@PathVariable String sortDir) {
        try {
            List<PostDTO> mapList = new ArrayList<>();
            List<Post> postList = new ArrayList<>();
            if(sortDir.equals("desc")) postList = postRepository.findAll(Sort.by(Sort.Direction.DESC, "tags"));
            else postList = postRepository.findAll(Sort.by(Sort.Direction.ASC, "tags"));
            for (Post post: postList
            ) {
                mapList.add(postService.mapToPostDTO(post));
            }
            return ResponseEntity.ok(mapList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
    @GetMapping("/all/tags/{sortDir}")
    public ResponseEntity<List<PostDTO>> getAllPostsTd(@PathVariable String sortDir) {
        try {
            List<PostDTO> mapList = new ArrayList<>();
            List<Post> postList = new ArrayList<>();
            if(sortDir.equals("desc")) postList = postRepository.findAll(Sort.by(Sort.Direction.DESC, "tags"));
            else postList = postRepository.findAll(Sort.by(Sort.Direction.ASC, "tags"));
            for (Post post: postList
            ) {
                mapList.add(postService.mapToPostDTO(post));
            }
            return ResponseEntity.ok(mapList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @PostMapping("/add")
    public ResponseEntity<?> addPost(@RequestBody PostDTO postDTO) {
        try {
            for (String username: postDTO.getAuthors()
                 ) {
                authorRepository.findByUsername(username)
                        .orElseThrow(() ->
                                new ResouceNotFoundException("User not found for username:" + username));
            }
            Post post = mapService.mapToPost(postDTO);
            postRepository.save(post);
            Map<String, Boolean> map = new HashMap<>();
            map.put("success", true);
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    @PutMapping("/edit/{postId}")
    public ResponseEntity<?> updatePost(@PathVariable(value = "postId") long postId, @RequestBody PostDTO postDTO) {
        try {
            Post post = postService.findById(postId);
            postService.update(post,postDTO);
            postRepository.save(post);
            Map<String, Boolean> map = new HashMap<>();
            map.put("success", true);
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/delete/{id}")
    @CacheEvict(value = "posts",allEntries = false, key = "#postId")
    public void deletePost(@PathVariable(value = "id") long postId) {
           Post post = postRepository.findById(postId).orElseThrow(() -> new ResouceNotFoundException("Post not found for id: " + postId));
           postRepository.delete(post);
    }
    @RequestMapping(path = "addTag/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PostDTO> addTag (@RequestParam("tag") String tag, @PathVariable("id") long id){
        return postService.addTagToPost(id,tag)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }
}

