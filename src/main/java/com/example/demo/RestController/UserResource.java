package com.example.demo.RestController;

import com.example.demo.DTO.AuthorDTO;
import com.example.demo.DTO.CommentDTO;
import com.example.demo.DTO.PostDTO;
import com.example.demo.ResouceNotFoundException;
import com.example.demo.model.Author;
import com.example.demo.model.Post;
import com.example.demo.repository.AuthorRepository;
import com.example.demo.repository.PostRepository;
import com.example.demo.service.AuthorService;
import com.example.demo.service.CommentService;
import com.example.demo.service.PostService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import javax.persistence.Cacheable;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
@Cacheable
public class UserResource {

    private final AuthorRepository authorRepository;
    private PostService postService;
    private CommentService commentService;
    private PostRepository postRepository;
    private AuthorService authorService;


    @PostMapping("/registry")
    public ResponseEntity<?> registry(@RequestBody AuthorDTO authorDTO) {
        try {
            Author author = authorService.mapToDtoAuthor(authorDTO);
            authorRepository.save(author);
            Map<String, String> map = new HashMap<>();
            map.put("success", "registration success");
            return new ResponseEntity<>(map, HttpStatus.OK);
        }catch (Exception e)
        {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
    @RequestMapping(path = "/UsersPosts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PostDTO> getUserPosts (@RequestParam String username) {
        return postService.getAllUserPosts(username)
                .stream()
                .map(ResponseEntity::ok)
                .findAny().orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }
    @RequestMapping(path = "/UserComments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CommentDTO> getUserComments (@RequestParam String username) {
        return commentService.getAllUserComments(username)
                .stream()
                .map(ResponseEntity::ok)
                .findAny().orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }
    @RequestMapping(path = "/UserCoAuthors", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<AuthorDTO>> getCoAuthors (@RequestParam String username) {
        try {
            Author author = authorRepository.findByUsername(username)
                    .orElseThrow(() -> new ResouceNotFoundException("Username not found: " + username));

            List<Post> postOfAuthor = postRepository
                    .findAll().stream().filter(x -> x.getAuthors()
                            .contains(author))
                    .collect(Collectors.toList());

            Set<Author> coAuthors = new HashSet<>();
            for (Post post : postOfAuthor) {
                coAuthors.addAll(post.getAuthors());
            }
            coAuthors.remove(author);

            Set<AuthorDTO> coAuthorsDTO =
                    coAuthors.stream().map(x -> authorService.mapToAuthorDto(x))
                    .collect(Collectors.toSet());

         return  ResponseEntity.ok(coAuthorsDTO);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }
    @RequestMapping(path = "/PostsWithWord/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PostDTO>> howManyTimes(@RequestParam String word, @PathVariable String username) {
       try {

           List<Post> posts = postRepository.findByWordLike(word);

           Author author = authorRepository.findByUsername(username)
                   .orElseThrow(() -> new ResouceNotFoundException("Username not found: " + username));

           List<Post> postOfAuthor = postRepository
                   .findAll().stream().filter(x -> x.getAuthors()
                           .contains(author))
                   .collect(Collectors.toList());

           List<PostDTO> postDTOS = postOfAuthor.stream()
                   .map(x -> postService.mapToPostDTO(x)).collect(Collectors.toList());

           return ResponseEntity.ok(postDTOS);
       }catch (Exception e)
       {
           e.printStackTrace();
           return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
       }
    }
}