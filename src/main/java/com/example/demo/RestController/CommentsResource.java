package com.example.demo.RestController;

import com.example.demo.DTO.CommentDTO;
import com.example.demo.model.Comment;
import com.example.demo.repository.CommentRepository;
import com.example.demo.service.CommentService;
import com.example.demo.service.MapService;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/comment")
@AllArgsConstructor
public class CommentsResource {
    private CommentService commentService;
    private CommentRepository commentRepository;
    private MapService mapService;

    @PostMapping("/add/{postId}")
    public ResponseEntity<?> addComment(@PathVariable(value = "postId") long postId,
                                              @RequestBody CommentDTO commentDTO) {
        try {
            commentDTO.setPostId(postId);
            Comment comment = mapService.mapToComment(commentDTO);
            commentRepository.save(comment);
            Map<String, Boolean> map = new HashMap<>();
            map.put("success", true);
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/edit/{commentId}")
    public ResponseEntity<?> updateComment(@PathVariable(value = "commentId") long commentId,
                                           @RequestBody CommentDTO commentDTO) {
        try {
            Comment comment = commentService.findById(commentId);
            comment.setUsername(commentDTO.getUsername());
            comment.setContent(commentDTO.getContent());
            Map<String, Boolean> map = new HashMap<>();
            map.put("success", true);
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/delete/{commentId}")
    @CacheEvict(value = "comments",allEntries = false, key = "#commentId")
    public ResponseEntity<Object> deleteEmployee(@PathVariable(value = "commentId") long commentId) {
        try {
            commentRepository.findById(commentId)
                    .ifPresent(existingPost -> commentRepository.delete(existingPost));
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
